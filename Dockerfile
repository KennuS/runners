FROM python 3.12.4-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME Summer
CMD ["python", "app.py"]